package com.app.asmabi.signatreradio;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.app.asmabi.signatreradio.Utils.FileNotFounEvent;
import com.app.asmabi.signatreradio.Utils.Myservice;
import com.app.asmabi.signatreradio.Utils.UserPref;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ImageView ButtonPlayPause, FacebookShare;
    ImageView btn_play, btn_pause;
    ProgressBar progressBar;
    String message;
    String resultCode = "0";
    private MediaPlayer mediaPlayer;
    Bundle bundle;
    UserPref up;
    Intent startPlay;
    String find;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_play = (ImageView) findViewById(R.id.btn_play);
        btn_pause = (ImageView) findViewById(R.id.btn_pause);
        bundle = new Bundle();
        startPlay = new Intent(getApplicationContext(), Myservice.class);
//        if (isMyServiceRunning(Myservice.class)) {
//            btn_pause.setVisibility(View.VISIBLE);
//            btn_play.setVisibility(View.GONE);
//        } else {
//
//        }
//String find;
        find = getIntent().getStringExtra("value");
        Toast.makeText(this, ""+find, Toast.LENGTH_SHORT).show();
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        up = new UserPref(getApplicationContext());

        initView();
        final String message = "This link is shared";
        FacebookShare = (ImageView) findViewById(R.id.facebookShare);


        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bounce);

        FacebookShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);
                startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        new GetData().execute();
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_play.startAnimation(animation);
//                getDataObject.doInBackground();
                if (resultCode.equals("1")) {
                    startPlay.putExtra("PausePlay", "play");
                    startService(startPlay);
                    btn_play.setVisibility(View.GONE);
                    btn_pause.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(MainActivity.this, "Something went wrong. Please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_pause.startAnimation(animation);
                //stopService(new Intent(getApplicationContext(), Myservice.class));
                //mediaPlayer.pause();

                startPlay.putExtra("PausePlay", "pause");
                startService(startPlay);
                btn_play.setVisibility(View.VISIBLE);
                btn_pause.setVisibility(View.GONE);
            }
        });
    }

    public boolean isConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }

        return false;
    }

    private void initView() {
        ButtonPlayPause = (ImageView) findViewById(R.id.btn_play);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        mediaPlayer = new MediaPlayer();

        String stateRunning = up.GetPlayerState();

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.listen_live) {
        } else if (id == R.id.website) {
            Intent intent = new Intent(MainActivity.this, PageOneActivity.class);
            startActivity(intent);
        } else if (id == R.id.aboutUs) {
            Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private boolean isMyServiceRunning(Class serviceClass) {
        int a = 0;
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                a = 1;
            }
        }
        if (a == 1) {
            return true;
        } else {
            return false;
        }
    }

    @Subscribe
    public void NavigateVlue(FileNotFounEvent event) {
        Toast.makeText(this, "something went wrong", Toast.LENGTH_SHORT).show();


    }

    class GetData extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection urlConnection = null;
            String result = "";
            try {
                URL url = new URL("http://ca4.rcast.net:8066/;stream.mp3");
//                URL url = new URL("http://vprbbc.streamguys.net:80/vprbbc24.mp3");
                urlConnection = (HttpURLConnection) url.openConnection();

                int code = urlConnection.getResponseCode();

                if (code == 200) {
                    resultCode = "1";
                }

                return "1";
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                urlConnection.disconnect();
            }
            return result;

        }

    }
}
