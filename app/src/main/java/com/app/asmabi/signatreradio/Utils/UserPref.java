package com.app.asmabi.signatreradio.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by hp on 10-02-2017.
 */

public class UserPref {
    SharedPreferences sp;
    Context activity;

    public UserPref(Context context) {
        activity = context;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void SavePlayerState(String state) {
        SharedPreferences.Editor sta = sp.edit();
        sta.putString("playing", state);
        sta.commit();
    }
    public String GetPlayerState() {
        String state = sp.getString("playing", "0");
        return state;
    }


}
