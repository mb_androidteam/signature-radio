package com.app.asmabi.signatreradio.Utils;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.os.SystemClock;
import android.widget.RemoteViews;

import com.app.asmabi.signatreradio.MainActivity;
import com.app.asmabi.signatreradio.R;

/**
 * Created by ADMIN on 13-02-2017.
 */

public class Myservice extends Service {
    private static final String TAG = null;
    MediaPlayer player;
    static final String AUDIO_PATH = "http://ca4.rcast.net:8066/;stream.mp3";
//    static final String AUDIO_PATH ="http://vprbbc.streamguys.net:80/vprbbc24.mp3";
    int lastpos = 0;

    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();


        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.putExtra("value","pased");//testing-> how value is reached activity
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        RemoteViews notificationView = new RemoteViews(this.getPackageName(), R.layout.notification);
        Intent buttonCloseIntent = new Intent(this, NotificationCloseButtonHandler.class);
        buttonCloseIntent.putExtra("action", "close");
        PendingIntent buttonClosePendingIntent = pendingIntent.getBroadcast(this, 0, buttonCloseIntent, 0);
        notificationView.setOnClickPendingIntent(R.id.notification_button_close, buttonClosePendingIntent);
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.icon);
        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this)
                .setContentTitle("Radio Signature")
                .setTicker("Radio Signature")
                .setContentText("Radio Signature")
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(
                        Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContent(notificationView)
                .setOngoing(true).build();
        startForeground(1337, notification);

        try {
            Uri bnbnb = Uri.parse(AUDIO_PATH);

            player = MediaPlayer.create(this, Uri.parse(AUDIO_PATH));
            if (player == null) {
                BusFactory.getBus().post(new FileNotFounEvent());
            } else {
                player.setLooping(true); // Set looping
                player.setVolume(100, 100);
                player.start();
            }
        } catch (Exception e) {
            BusFactory.getBus().post(new FileNotFounEvent());
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        String ans = intent.getStringExtra("PausePlay");
        try {
            if (ans.equals("pause")) {
                lastpos = player.getCurrentPosition();
                player.pause();
            } else {
                if (ans.equals("play")) {
                    try {
                        player.seekTo(lastpos);
                        player.start();
                    } catch (Exception e) {
                        BusFactory.getBus().post(new FileNotFounEvent());
                        System.out.println("jaedf" + e);
                    }
                }
            }
        } catch (Exception e) {
            BusFactory.getBus().post(new FileNotFounEvent());
            System.out.println("j" + e);
        }
        return START_NOT_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }

    @Override
    public void onDestroy() {
        player.stop();
        player.release();
    }


    @Override
    public void onLowMemory() {

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getApplicationContext(), this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 1000, restartServicePI);
    }

}