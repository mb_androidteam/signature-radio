package com.app.asmabi.signatreradio;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.app.asmabi.signatreradio.Utils.BusFactory;

/**
 * Created by Sanif on 03-01-2016.
 */
public class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);
    }


}
