package com.example.minusbug.onlineradio.ExternalLibraries;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.example.minusbug.onlineradio.R;

/**
 * Created by minusbug on 01/13/2017.
 */

public class PageOneActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_one);
        getSupportActionBar().hide();
        ImageView back = (ImageView) findViewById(R.id.back);
//        WebView mWebview  = new WebView(this);
//
//        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
//
//        final Activity activity = this;
//
//        mWebview.setWebViewClient(new WebViewClient() {
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        mWebview .loadUrl("http://www.google.com");
//        setContentView(mWebview );
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}
