package com.example.minusbug.onlineradio;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.minusbug.onlineradio.ExternalLibraries.PageOneActivity;

import me.angrybyte.circularslider.CircularSlider;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnCompletionListener {
    ImageView ButtonPlayPause;
    public EditText editTextSongURL;
    CircularSlider seekbar_circular;
    private MediaPlayer mediaPlayer;
    private final Handler handler = new Handler();
    ProgressBar progressBar;
    private boolean pauseState = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekbar_circular = (CircularSlider) findViewById(R.id.seekbar_circular);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initView();

        final Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bounce);


        ButtonPlayPause.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ButtonPlayPause.startAnimation(animation);
//                Toast.makeText(getApplicationContext(), "cliked", Toast.LENGTH_SHORT).show();
                if (isConnectedToInternet()) {
                    if (pauseState == true) {
                        mediaPlayer.start();
                        ButtonPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.button_pause));
                        pauseState = false;
                    } else {
                        if (!mediaPlayer.isPlaying()) {
//                        pd.show();
                            progressBar.setVisibility(View.VISIBLE);
                            editTextSongURL.setText("http://streaming.shoutcast.com/radiomes?lang=en-US%2cen%3bq%3d0.8");
                            try {
                                mediaPlayer.setDataSource(editTextSongURL.getText().toString()); // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source

                                mediaPlayer.prepareAsync();
                            } catch (Exception e) {
                                pauseState = false;
                                mediaPlayer.stop();
                                ButtonPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.button_play));
//                            pd.dismiss();
                                progressBar.setVisibility(View.GONE);
//                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {
                                    mediaPlayer.start();
                                    pauseState = false;
//                                pd.dismiss();
                                    progressBar.setVisibility(View.GONE);

                                }

                            });
                            ButtonPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.button_pause));
                        } else {
                            mediaPlayer.pause();
                            pauseState = true;
                            ButtonPlayPause.setImageDrawable(getResources().getDrawable(R.drawable.button_play));
                        }
                    }
                }
                else
                {
                    // Here I've been added intent to open up data settings
                    Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public boolean isConnectedToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    private void initView() {
        ButtonPlayPause = (ImageView) findViewById(R.id.btn_play);
        editTextSongURL = (EditText) findViewById(R.id.EditTextSongURL);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        mediaPlayer = new MediaPlayer();

    }


    @Override
    public void onCompletion(MediaPlayer mp) {
        /** MediaPlayer onCompletion event handler. Method which calls then song playing is complete*/
        mediaPlayer.stop();
        ButtonPlayPause.setImageResource(R.drawable.button_play);
        pauseState = false;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_gallery) {
            Intent intent = new Intent(MainActivity.this, PageOneActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {
            Intent intent = new Intent(MainActivity.this, PageOneActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_send) {
            Intent intent = new Intent(MainActivity.this, AboutUsActivity.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
