package com.example.minusbug.onlineradio;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import java.util.List;

/**
 * Created by Sanif on 03-01-2016.
 */
public class BaseActivity extends AppCompatActivity {

    private OnBackPressedListener backPressedListener;
    //TODO: handle other permission groups

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void setOnBackPressedListener(OnBackPressedListener listener) {
        backPressedListener = listener;
    }

    /**
     * Method to add fragment to the container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(int containerId, Fragment fragment, boolean addToBackStack) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.add(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();
    }

    /**
     * Method to add fragment to the container with id R.id.container.
     *
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
//    public void addFragment(Fragment fragment, boolean addToBackStack) {
//        addFragment(R.id.container, fragment, addToBackStack);
//    }

    /**
     * Method to replace fragment in a container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to replace.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void replaceFragment(int containerId, Fragment fragment, boolean addToBackStack, boolean animate, int animtype) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        if (animate) {
            //check type o nimation
            switch (animtype) {
                case 0:

                    break;
                case 1:

                    break;

            }
        }
        fTransaction.replace(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();

    }

//    public void replaceFragment(Fragment fragment, boolean addToBackStack, boolean animate, int animtype) {
//        replaceFragment(R.id.container, fragment, addToBackStack, animate, animtype);
//    }

    public Fragment getFragment(Class clazz) {
        return getSupportFragmentManager().findFragmentByTag(clazz.getSimpleName());
    }

    /**
     * To check whether the given fragment is the current fragment.
     *
     * @param fragment
     * @return
     */
    public boolean isCurrentFragment(Fragment fragment) {
        Fragment tFragment = getFragment(fragment.getClass());
        if (tFragment != null && tFragment.isVisible()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //to handel the actionbar back button
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    //Nested fragments backstack workaround
    private boolean onBackPressed(FragmentManager fm) {
        if (fm != null) {

            try {
                if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            List<Fragment> fragList = fm.getFragments();
            if (fragList != null && fragList.size() > 0) {
                for (Fragment frag : fragList) {
                    if (frag == null) {
                        continue;
                    }
                    if (frag.isVisible()) {
                        if (onBackPressed(frag.getChildFragmentManager())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (backPressedListener == null || !backPressedListener.onBackPressed()) {

            FragmentManager fm = getSupportFragmentManager();
            if (onBackPressed(fm)) {
                return;
            }
            super.onBackPressed();
        }
    }

    public interface OnBackPressedListener {
        boolean onBackPressed();
    }

}
